﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class ImageTargetScript : MonoBehaviour {

	public bool isThisTargetActive;
	public ImageTargetScript anotherTarget;

	ParticleSpawnerScript spawnerScript;
	GameObject spheres;

	void Start () 
	{
		spheres = GetComponentInChildren<UVRandomizer>().gameObject;
		spawnerScript = GetComponentInChildren<ParticleSpawnerScript>();
		spawnerScript.targetForEmiting = anotherTarget.gameObject;
		//подпись на ивенты vuforia
		GetComponent<DefaultTrackableEventHandler>().OnTrackingFoundEvent += () => isThisTargetActive = true;
		GetComponent<DefaultTrackableEventHandler>().OnTrackingLostEvent += () => isThisTargetActive = false;
	}

	void Update()
	{
		//включает/отключает создание частиц в зависимости от другого маркера
		if(anotherTarget.isThisTargetActive) {spawnerScript.isEmitting = true;} else {spawnerScript.isEmitting = false;}
	}

	public void SpheresSetActive(bool value)
	{
		spheres.SetActive(value);
	}

}
