﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceControllerScript : MonoBehaviour {

	public int minDistanceInMM = 80;
	public TextMesh textMesh;

	public ImageTargetScript firstTarget;
	public ImageTargetScript secondTarget;

	int distanceInMM;

	void Start () 
	{
		
	}

	void Update () 
	{
		//измеряет дистанцию и обновляет текст
		if(firstTarget.isThisTargetActive && secondTarget.isThisTargetActive)
		{
			transform.position = (firstTarget.transform.position + secondTarget.transform.position)/2;
			distanceInMM = (int)(Vector3.Distance(firstTarget.transform.position, secondTarget.transform.position) * 1000f);
			textMesh.text = distanceInMM.ToString() + "mm";
		}
		else
		{
			textMesh.text = "";
		}
			

		//включает и отключает сферы
		if(distanceInMM <= minDistanceInMM)
		{
			firstTarget.SpheresSetActive(true);
			secondTarget.SpheresSetActive(true);
		}
		else
		{
			firstTarget.SpheresSetActive(false);
			secondTarget.SpheresSetActive(false);
		}

	}
}
