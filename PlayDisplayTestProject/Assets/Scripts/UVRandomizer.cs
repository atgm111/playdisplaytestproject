﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UVRandomizer : MonoBehaviour
{
	public MeshFilter[] meshes;
	public GameObject meshPrefab;

	void Start()
	{
		meshes = GetComponentsInChildren<MeshFilter>();
		foreach(MeshFilter meshForRandomize in meshes)
		{
			RandomizeUV(meshForRandomize.mesh);
		}
		CombainMeshes();
	}

	//создает случайную UV, каждый меш будет выглядеть иначе
	void RandomizeUV(Mesh mesh)
	{
		Vector3[] vertices = mesh.vertices;
		Vector2[] uvs = new Vector2[vertices.Length];

		for (int i = 0; i < uvs.Length; i++)
		{
			uvs[i] = new Vector2(vertices[i].x + Random.Range(-1f, 1f), vertices[i].z + Random.Range(-1f, 1f));
		}
		mesh.uv = uvs;

	}

	//комбинирует все меши сфер в один, отключает оригинальные сферы и создает новый объект с комбинированным мешем
	void CombainMeshes()
	{
		
		CombineInstance[] combine = new CombineInstance[meshes.Length];
		int i = 0;

		while (i < meshes.Length) 
		{
			combine[i].mesh = meshes[i].sharedMesh;
			combine[i].transform = Matrix4x4.TRS(meshes[i].transform.localPosition, meshes[i].transform.rotation, meshes[i].transform.localScale);
			meshes[i].gameObject.SetActive(false);
			i++;
		}
			
		GameObject newMesh = Instantiate(meshPrefab, transform);

		newMesh.transform.GetComponent<MeshFilter>().mesh = new Mesh();
		newMesh.transform.GetComponent<MeshFilter>().mesh.CombineMeshes(combine, true);
		newMesh.transform.GetComponent<MeshRenderer>().material = meshes[0].GetComponent<MeshRenderer>().material;
		newMesh.transform.localPosition = Vector3.zero;

	}
}