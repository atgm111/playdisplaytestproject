﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSpawnerScript : MonoBehaviour {

	public bool isEmitting;
	public GameObject targetForEmiting;
	ParticleSystem thisParticleSystem;

	void Start () 
	{
		thisParticleSystem = GetComponent<ParticleSystem>();
		thisParticleSystem.trigger.SetCollider(0, targetForEmiting.GetComponent<Collider>());
	}

	//включение и отключение создания новых частиц
	void ParticleSystemEmittingController() 
	{
		if(isEmitting && !thisParticleSystem.isPlaying) thisParticleSystem.Play();
		if(!isEmitting && thisParticleSystem.isPlaying) thisParticleSystem.Stop(); 
	}

	void Update () 
	{
		ParticleSystemEmittingController();
		transform.LookAt(targetForEmiting.transform);
	}


}
